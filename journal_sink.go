package journal

import (
	"fmt"
	"strings"

	"gitlab.com/go-figure/logr"
)

var _ logr.Sink = JournalSink{}

type JournalSink struct {
	identifier string
}

func NewJournalSink(identifier string) JournalSink {
	return JournalSink{identifier: identifier}
}

func (s JournalSink) Event(event string, kv logr.KV) {
	message := fmt.Sprintf("event:%s %s\n", event, logr.FormattedKV(kv))
	Send(message, PriorityInfo, journalFields(s.identifier, event, kv))
}

func journalFields(identifier, event string, kv logr.KV) map[string]string {
	fields := map[string]string{
		"EVENT": event,
	}
	if identifier != "" {
		fields["SYSLOG_IDENTIFIER"] = identifier
	}
	for key, val := range kv {
		key = strings.ReplaceAll(key, ".", "_")
		key = strings.ToUpper(key)
		key = "KV_" + key

		fields[key] = logr.FormatField(key, val)
	}
	return fields
}
